# Laboratory nr.2 - Flocking Behaviour
#### Table of contents
* [Introduction](#introduction)
* [Types of behaviour](#types-of-behaviour)
* [Technologies](#technologies)
* [Setup](#setup)
* [Demo](#demo)
* [Resources](#resources)

## Introduction
The main task of laboratory work nr.2 is to add intelligence to the NPCs. Thus, they are able to simulate close to real life behaviour depending on their state. Also, it should provide logical feedback based on the changes in environment. 
<br> In order to trigger a behavior change of the boids, the user can toggle one of the 3 available buttons. The state and status(ON/OFF) is presented above the buttons.

## Types of behaviour
Current project contains 3 types of behaviour:
* Flocking - the normal behaviour of the boids.
  <br>It implies such steering behaviours as:
    - Alignment
    - Cohesion
    - Separation
* Evading - avoiding threats, such as: missiles, ship. Also, it implies flocking behaviour.
* Attacking - throwing boids in order to collide and destroy the ship. Also, it implies evading collision between boids.

## Technologies
Used technologies:
* Python 3.7
* simpleui module
* Codeskulptor

## Setup
In order to run the project, the following steps should be done:
1. Apply the chages from _behavior.py_ to the base code
2. Run the code in Codeskulptor
3. Enjoy :)

## Demo
[Project demo](https://drive.google.com/file/d/12TxEOe4opJylkMW_uHbaOeIuqmEFN463/view?usp=sharing)

## Resources
UTM Fundamentals of Artificial Intelligence Course
