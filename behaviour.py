# ...
evading_force = 3
crowd_radix = 100
avoidance_radius = 80
max_avoidance_force = const_rock_speed
max_velocity = 2
boid_options = [True, False, False]
attacking_radius = 400


def object_dist(rock1, rock2):
    return dist(rock1.pos, rock2.pos)


def vector_len(v):
    return math.sqrt(v[0] ** 2 + v[1] ** 2)


def normalize(v, thickness=1.0):
    v_len = vector_len(v)
    if v_len == 0:
        return [0, 0]
    return [e / v_len * thickness for e in v]


def compute_alignment(rock, rocks):
    v = [0, 0]
    neighbor_count = 0
    for r in rocks:
        if r != rock and object_dist(r, rock) < crowd_radix:
            v[0] += r.vel[0]
            v[1] += r.vel[1]
            neighbor_count += 1
    if neighbor_count > 0:
        v[0] /= neighbor_count
        v[1] /= neighbor_count
    return normalize(v)


def compute_cohesion(rock, rocks):
    v = [0, 0]
    neighbor_count = 0
    for r in rocks:
        if r != rock and object_dist(r, rock) < crowd_radix:
            v[0] += r.pos[0]
            v[1] += r.pos[1]
            neighbor_count += 1
    if neighbor_count > 0:
        v[0] /= neighbor_count
        v[1] /= neighbor_count
    v = [v[0] - rock.pos[0], v[1] - rock.pos[1]]
    return normalize(v)


def compute_separation(rock, rocks):
    v = [0, 0]
    neighbor_count = 0
    for r in rocks:
        dst = object_dist(r, rock)
        if r != rock and crowd_radix > dst > 0:
            v[0] += (rock.pos[0] - r.pos[0]) / dst
            v[1] += (rock.pos[1] - r.pos[1]) / dst
            neighbor_count += 1
    if neighbor_count > 0:
        v[0] /= neighbor_count
        v[1] /= neighbor_count
    return normalize(v)


def vector_add(v1, v2):
    return [x1 + x2 for x1, x2 in zip(v1, v2)]


def vector_sub(v1, v2):
    return [x1 - x2 for x1, x2 in zip(v1, v2)]


def collision_avoidance(rock):
    dynamic_length = vector_len(rock.vel)
    ahead = vector_add(normalize(rock.vel, dynamic_length), rock.pos)
    ahead2 = vector_add(normalize(rock.vel, dynamic_length * 0.5), rock.pos)
    most_threatening = find_most_threatening_obstacle(rock, ahead, ahead2)
    avoidance = [0, 0]

    if most_threatening:
        avoidance[0] = ahead[0] - most_threatening.pos[0]
        avoidance[1] = ahead[1] - most_threatening.pos[1]
        avoidance = normalize(avoidance, evading_force)
    return avoidance if most_threatening else None


def line_intersects_circle(ahead, ahead2, missile, radius):
    return dist(missile.pos, ahead) <= radius or dist(missile.pos, ahead2) <= radius


def find_most_threatening_obstacle(rock, ahead, ahead2):
    most_threatening = None
    for missile in missile_group:
        collision = line_intersects_circle(ahead, ahead2, missile, avoidance_radius)
        if collision and (not most_threatening or object_dist(rock, missile) < object_dist(rock, most_threatening)):
            most_threatening = missile
    collision = line_intersects_circle(ahead, ahead2, my_ship, avoidance_radius * 2)
    if collision and (not most_threatening or object_dist(rock, my_ship) < object_dist(rock, most_threatening)):
        most_threatening = my_ship
    return most_threatening


def attacking(rock):
    if object_dist(rock, my_ship) < attacking_radius:
        desired_velocity = normalize(vector_sub(my_ship.pos, rock.pos), max_velocity)
        steering = vector_sub(desired_velocity, rock.vel)
        return normalize(steering, max_velocity)
    return None


def get_or_0(v):
    return v if v else [0, 0]


def update_rock_group_pos(rocks):
    for rock in rocks:
        a = c = f = None
        if boid_options[2]:
            a = attacking(rock)
        if boid_options[1]:
            c = collision_avoidance(rock)
        if boid_options[0]:
            f = flocking(rock, rocks, a is not None or c is not None)
        rock.vel = vector_add(vector_add(rock.vel, get_or_0(a)), get_or_0(f))
        rock.vel = normalize(rock.vel, max_velocity)
        rock.vel = vector_add(rock.vel, get_or_0(c))


def flocking(rock, rocks, separation_only=False):
    alignment = [0, 0]
    cohesion = [0, 0]
    if not separation_only:
        cohesion = compute_cohesion(rock, rocks)
        alignment = compute_alignment(rock, rocks)
    separation = compute_separation(rock, rocks)
    return [alignment[0] + cohesion[0] + separation[0] * 1.5,
            alignment[1] + cohesion[1] + separation[1] * 1.5]

# ...


asteroid_info = ImageInfo([45, 45], [20, 20], 30)


# ...

def draw(canvas):
    # ...
    # my_ship.update()
    update_rock_group_pos(rock_group)
    # process_sprite_group(rock_group, canvas)
    # ...

# ...


# Controls
def set_option(idx, val):
    boid_options[idx] = val


def option_click(option_idx, label):
    texts = label.get_text().split()
    if texts[1] == 'ON':
        set_option(option_idx, False)
        texts[1] = 'OFF'
    else:
        set_option(option_idx, True)
        texts[1] = 'ON'
    label.set_text(' '.join(texts))


label1 = frame.add_label('Flocking ON')
label2 = frame.add_label('Evading OFF')
label3 = frame.add_label('Attacking OFF')

frame.add_button("Toggle Flocking", lambda: option_click(0, label1), 150)
frame.add_button("Toggle Evading", lambda: option_click(1, label2), 150)
frame.add_button("Toggle Attacking", lambda: option_click(2, label3), 150)

# frame.start()
